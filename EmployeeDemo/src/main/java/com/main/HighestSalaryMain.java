package com.main;

/**
 * 
 * @author iranna
 *
 */
import com.model.Employee;
import com.service.EmployeeService;

public class HighestSalaryMain {

	public static void main(String[] args) {
		Employee employee = new Employee(22, "Hello", 5000f);
		Employee employee1 = new Employee(32, "Hi", 10000f);

		EmployeeService employeeService = new EmployeeService();

		Employee obj = employeeService.highsalary(employee, employee1);
		System.out.println("Employee details who is getting highest salary is");
		System.out.println("Employee number:" + obj.getEmployeeId());
		System.out.println("Employeename:" + obj.getEmployeeName());
		System.out.println("slary:" + obj.getEmployeeSalary());

		employee = null;
		employee1 = null;
	}

}
