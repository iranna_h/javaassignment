
package com.main;

/**
 * 
 * @author iranna
 *
 */
import com.model.Employee;
import com.service.EmployeeService;

public class EmployeeSearchMain {

	public static void main(String[] args) {

		Employee employee1 = new Employee(10, "Iranna", 10000f);
		Employee employee2 = new Employee(20, "Ravi", 20000f);
		Employee employee3 = new Employee(10, "Ramesh", 30000f);

		EmployeeService employeeService = new EmployeeService();

		Employee[] employees = new Employee[3];
		employees[0] = employee1;
		employees[1] = employee2;
		employees[2] = employee3;

		Employee employeeDetail = employeeService.searchEmployeeByEmployeeName(employees, "Ravi");
		if (employeeDetail != null) {
			System.out.println("Employee details:" + "\n EmployeeId: " + employeeDetail.getEmployeeId()
					+ "\n Employee Name:" + employeeDetail.getEmployeeName() + "\n Employee salary:"
					+ employeeDetail.getEmployeeSalary());

		} else {
			System.out.println("Employee does mot exist");
		}

		employee1 = null;
		employee2 = null;
		employee3 = null;
		employees = null;
		employeeService = null;

	}

}
