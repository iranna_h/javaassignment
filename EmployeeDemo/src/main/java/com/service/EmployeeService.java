package com.service;

/**
 * 
 * @author iranna
 *
 */

import com.model.Employee;

public class EmployeeService {
	public Employee highsalary(Employee employee, Employee employee1) {
		if (employee.getEmployeeSalary() > employee.getEmployeeSalary())
			return employee;
		else
			return employee1;
	}

	public float calculatingEmployeeTaotalSalary(Employee[] employees) {
		float sum = 0;
		for (int i = 0; i < employees.length; i++) {
			sum = sum + employees[i].getEmployeeSalary();
		}
		return sum;
	}

	public Employee searchEmployeeByEmployeeName(Employee[] employees, String employeeName) {
		for (int i = 0; i < employees.length; i++) {
			if (employees[i].getEmployeeName().equals(employeeName)) {
				return employees[i];
			}

		}
		return null;
	}

}
