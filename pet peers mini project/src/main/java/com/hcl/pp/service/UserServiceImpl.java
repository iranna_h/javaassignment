package com.hcl.pp.service;

import java.util.Set;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.pp.exception.PetPeersException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;
import com.hcl.pp.repository.PetRepository;
import com.hcl.pp.repository.UserRepository;
import com.hcl.pp.validator.LoginValidator;
import com.hcl.pp.validator.UserValidator;

@Service

public class UserServiceImpl implements UserService {
	@Autowired
	UserRepository userRepository;
	@Autowired
	PetRepository petRepository;
	@Autowired
	PetService petService;
	private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);

	@Override
	@Transactional(rollbackOn = { Exception.class, RuntimeException.class, Error.class })
	public User addUser(UserValidator userValidator) throws PetPeersException {
		User userCreated = null;
		User user = null;
		if (!(userValidator.getUserPassword().equals(userValidator.getConfirmPassword()))) {
			throw new PetPeersException("User Password and Confirm Password do not match");
		} else {
			User userExistsWithUserName = userRepository.findByUserName(userValidator.getUserName());
			if (userExistsWithUserName != null) {
				throw new PetPeersException("The User Name already exists. Please set a different User Name");
			} else {
				try {
					user = new User();
					user.setUserName(userValidator.getUserName());
					user.setUserPassword(userValidator.getUserPassword());

					userCreated = userRepository.save(user);
					if (userCreated != null) {
						return userCreated;
					} else {
						throw new PetPeersException("User Not created");
					}
				} catch (Exception e) {
					logger.error("{}", e.getMessage());
				}
			}
			return userCreated;
		}
	}

	@Override
	@Transactional(rollbackOn = { Exception.class, RuntimeException.class, Error.class })
	public Set<Pet> loginUser(LoginValidator loginValidator) throws PetPeersException {
		User userExists = userRepository.findByUserNameAndUserPassword(loginValidator.getUserName(),
				loginValidator.getUserPassword());
		Set<Pet> pets = null;
		if (userExists != null) {

			pets = petService.getAllPets();
			if (pets != null && pets.size() > 0) {
				return pets;
			} else {
				throw new PetPeersException("No Pets Available");
			}
		} else {
			throw new PetPeersException("Either User Name, Password or both are invalid");
		}
	}

	@Override
	public Pet buyPet(Long userId, Long petId) throws PetPeersException {
		User userCustomer = userRepository.findById(userId).get();
		if (userCustomer != null) {
			Pet pet = petService.getPetById(petId);
			if (pet != null) {
				pet.setOwner(userCustomer);
				petRepository.save(pet);
				return pet;
			} else {
				throw new PetPeersException("No pet exists with given pet id. Try buying some other pet");
			}
		} else {
			throw new PetPeersException("Invalid User");
		}
	}

	@Override
	@Transactional(rollbackOn = { Exception.class, RuntimeException.class, Error.class })
	public Set<Pet> getMyPets(Long userId) throws PetPeersException {
		User owner = userRepository.findById(userId).get();
		if (owner != null) {
			Set<Pet> myPets = petService.getPetsByOwnerId(owner);
			if (myPets != null && myPets.size() > 0) {
				return myPets;
			} else {
				throw new PetPeersException("You don't have Pets");
			}
		} else {
			throw new PetPeersException("Invalid User");
		}
	}

	@Override
	public User findByUserName(String userName) throws PetPeersException {

		User user = userRepository.findByUserName(userName);
		if (user != null) {
			return user;
		} else {
			throw new PetPeersException("No user exists with given user name");

		}
	}

	@Override
	public User getUserById(Long userId) throws PetPeersException {
		User user = userRepository.findById(userId).get();
		if (user != null) {
			return user;
		} else {
			throw new PetPeersException("No user exists with given user id");
		}
	}

	@Override
	public User updateUser(Long userId, UserValidator userValidator) throws PetPeersException {
		User userUpdated = null;
		User user = null;
		if ((userRepository.findById(userId) != null)
				&& !(userValidator.getUserPassword().equals(userValidator.getConfirmPassword()))) {
			throw new PetPeersException("User Password and Confirm Password do not match");
		} else {
			User userExistsWithUserName = userRepository.findByUserName(userValidator.getUserName());
			if (userExistsWithUserName != null) {
				throw new PetPeersException("The User Name already exists. Please set a different User Name");
			} else {
				try {
					user = new User();
					user.setUserName(userValidator.getUserName());
					user.setUserPassword(userValidator.getUserPassword());

					userUpdated = userRepository.save(user);
					if (userUpdated != null) {
						return userUpdated;
					} else {
						throw new PetPeersException("User Not created");
					}
				} catch (Exception e) {
					logger.error("{}", e.getMessage());
				}
			}
			return userUpdated;
		}
	}

	@Override
	@Transactional(rollbackOn = { Exception.class, RuntimeException.class, Error.class })
	public Set<User> listUsers() throws PetPeersException {
		Set<User> users = (Set<User>) userRepository.findAll();
		if (users != null && users.size() > 0) {
			return users;
		} else {
			throw new PetPeersException("No users found");
		}
	}

	@Override
	public void removeUser(Long userId) throws PetPeersException {
		User user = userRepository.findById(userId).get();
		if (user != null) {
			userRepository.deleteById(userId);
		} else {
			throw new PetPeersException("No user exists with given user id");
		}
	}
}
