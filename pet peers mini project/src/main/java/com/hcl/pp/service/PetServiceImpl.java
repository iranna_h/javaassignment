package com.hcl.pp.service;

import java.util.Set;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.pp.exception.PetPeersException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;
import com.hcl.pp.repository.PetRepository;
import com.hcl.pp.validator.PetValidator;

@Service
public class PetServiceImpl implements PetService {

	@Autowired
	PetRepository petRepository;

	@Override
	@Transactional(rollbackOn = { Exception.class, RuntimeException.class, Error.class })
	public Pet savePet(PetValidator petRequest) throws PetPeersException {
		Pet petCreated = null;
		ModelMapper modelMapper = new ModelMapper();
		Pet petRequested = modelMapper.map(petRequest, Pet.class);
		petCreated = petRepository.save(petRequested);
		if (petCreated != null) {
			return petCreated;
		} else {
			throw new PetPeersException("Pet Not created");
		}
	}

	@Override
	@Transactional(rollbackOn = { Exception.class, RuntimeException.class, Error.class })
	public Set<Pet> getAllPets() throws PetPeersException {
		Set<Pet> pets = (Set<Pet>) petRepository.findAll();
		if (pets != null && pets.size() > 0) {
			return pets;
		} else {
			throw new PetPeersException("No Pets Available");
		}
	}

	@Override
	@Transactional(rollbackOn = { Exception.class, RuntimeException.class, Error.class })
	public Set<Pet> getPetsByOwnerId(User owner) throws PetPeersException {
		Set<Pet> pets = petRepository.findByOwner(owner);
		if (pets != null && pets.size() > 0) {
			return pets;
		} else {
			throw new PetPeersException("You don't have Pets");
		}
	}

	@Override
	@Transactional(rollbackOn = { Exception.class, RuntimeException.class, Error.class })
	public Pet getPetById(Long petId) throws PetPeersException {
		Pet pet = petRepository.findById(petId).get();
		if (pet != null) {
			return pet;
		} else {
			throw new PetPeersException("No pet exists with given pet id");
		}
	}

	@Override
	@Transactional(rollbackOn = { Exception.class, RuntimeException.class, Error.class })
	public Pet savePetWithOwner(Pet petExists) throws PetPeersException {
		Pet petCreated = null;
		ModelMapper modelMapper = new ModelMapper();
		Pet petRequested = modelMapper.map(petExists, Pet.class);
		petCreated = petRepository.save(petRequested);
		if (petCreated != null) {
			return petCreated;
		} else {
			throw new PetPeersException("Pet Not created");
		}
	}

}
