package com.hcl.pp.validator;

import java.io.Serializable;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class PetValidator implements Serializable{
	
	
	private static final long serialVersionUID = 3441141445791878192L;

	@NotNull(message= "petName cannot be null")
	@NotEmpty(message= "petName cannot be empty")
	private String petName;
	
	@NotNull(message= "petAge cannot be null")
	@NotEmpty(message= "petAge cannot be empty")
	@Min(message= "Pet's age should be 0 and 99 years ", value = 0)
	@Max(message= "Pet's age should be 0 and 99 years ", value = 99)
	private int petAge;
	
	@NotNull(message= "petPlace cannot be null")
	@NotEmpty(message= "petPlace cannot be empty")
	private String petPlace;

	
}
