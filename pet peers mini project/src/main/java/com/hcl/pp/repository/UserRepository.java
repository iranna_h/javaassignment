package com.hcl.pp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.pp.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

	public abstract User findByUserName(String userName);

	public abstract User findByUserNameAndUserPassword(String userName, String password);

}