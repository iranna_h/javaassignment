package com.service;

/**
 * 
 * @author iranna
 *
 */
public class Employee implements Comparable<Employee> {
	private int empId;
	private String name;
	private float salary;

	public Employee() {
		super();
		
	}

	public Employee(int empId, String name, float salary) {
		super();
		this.empId = empId;
		this.name = name;
		this.salary = salary;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

	public int compareTo(Employee st) {
		if (salary == st.salary)
			return 0;
		else if (salary > st.salary)
			return 1;
		else
			return -1;
	}
}