package com.main;

import java.util.ArrayList;
import java.util.Collections;

import com.service.Employee;

/**
 * 
 * @author iranna
 *
 */
public class EmployeeMain {
	public static void main(String args[]) {
		ArrayList<Employee> al = new ArrayList<Employee>();
		al.add(new Employee(111, "Iranna", 1600f));
		al.add(new Employee(333, "Raja", 1800f));
		al.add(new Employee(222, "Rahul", 1200f));

		Collections.sort(al);
		for (Employee st : al) {
			System.out.println(st.getEmpId() + " " + st.getName() + " " + st.getSalary());
		}
	}
}
