package com.hcl.pppetmicroservice.service;


import java.util.Optional;
import java.util.Set;

import com.hcl.pppetmicroservice.exception.PetPeersException;
import com.hcl.pppetmicroservice.model.Pet;
import com.hcl.pppetmicroservice.validator.PetValidator;

public interface PetService {

	public Pet savePet(PetValidator petValidator) throws PetPeersException;

	public Set<Pet> getAllPets() throws PetPeersException;

	public Optional<Pet> getPetByPetId(Long petId) throws PetPeersException;

	public Set<Pet> getPetByUserId(Long userId) throws PetPeersException;
}

