package com.hcl.pppetmicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author iranna
 *
 */
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class PpPetMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PpPetMicroserviceApplication.class, args);
	}

}
