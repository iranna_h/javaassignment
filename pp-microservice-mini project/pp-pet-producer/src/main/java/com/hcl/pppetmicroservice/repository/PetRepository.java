package com.hcl.pppetmicroservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.pppetmicroservice.model.Pet;
@Repository
public interface PetRepository extends JpaRepository<Pet, Long> {

	public abstract List<Pet> findByUserId(Long userId);

}

