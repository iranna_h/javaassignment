package com.hcl.ppapigateway.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PetPeersFallBackController {

	@GetMapping(value = "/UserServiceFallBack")
	public ResponseEntity<String> userServiceFallbackMethod() {
		return new ResponseEntity<String>(
				"Fallback method called: Could not reach to User Service. Please try after some time",
				HttpStatus.GATEWAY_TIMEOUT);
	}

	@GetMapping(value = "/PetServiceFallBack")
	public ResponseEntity<String> petServiceFallbackMethod() {
		return new ResponseEntity<String>(
				"Fallback method called: Could not reach to Pet Service. Please try after some time",
				HttpStatus.GATEWAY_TIMEOUT);
	}
}
