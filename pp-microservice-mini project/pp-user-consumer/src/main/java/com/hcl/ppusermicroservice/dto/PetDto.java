package com.hcl.ppusermicroservice.dto;


import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PetDto {

	private Long id;

	private String name;

	private Integer age;

	private String place;

	private Long userId;

}

