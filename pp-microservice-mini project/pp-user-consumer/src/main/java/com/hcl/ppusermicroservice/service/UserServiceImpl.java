package com.hcl.ppusermicroservice.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.ppusermicroservice.dto.PetDto;
import com.hcl.ppusermicroservice.exception.PetPeersException;
import com.hcl.ppusermicroservice.feignclientinterface.PetClientInterface;
import com.hcl.ppusermicroservice.model.User;
import com.hcl.ppusermicroservice.repository.UserRepository;
import com.hcl.ppusermicroservice.validator.LoginValidator;
import com.hcl.ppusermicroservice.validator.UserValidator;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	PetClientInterface petClientInterface;

	private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);

	@Override
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class, Error.class })
	public User addUser(UserValidator userRequest) throws PetPeersException {
		User userCreated = null;

		if (!(userRequest.getUserPassword().equals(userRequest.getConfirmPassword()))) {
			throw new PetPeersException("Passwords do not match");
		} else {
			User userExistsWithUserName = userRepository.findByUserName(userRequest.getUserName());
			if (userExistsWithUserName != null) {
				throw new PetPeersException("User Name already in use. Please select a different User Name");
			} else {
				try {
					ModelMapper modelMapper = new ModelMapper();
					User userRequested = modelMapper.map(userRequest, User.class);
					userCreated = userRepository.save(userRequested);
					if (userCreated != null) {
						return userCreated;
					} else {
						throw new PetPeersException("User Not created");
					}
				} catch (PetPeersException e) {
					logger.error("{}", e.getMessage());
				}
			}
			return userCreated;
		}

	}

	@Override
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class, Error.class })
	public Set<PetDto> getMyPets(Long userId) throws PetPeersException {
		Optional<User> user = userRepository.findById(userId);
		if (user.isPresent()) {
			List<PetDto> myPets = new ArrayList<>();
			try {
				myPets = petClientInterface.getPetsByUserId(userId);
				if (myPets != null && myPets.size() > 0) {
					return (Set<PetDto>) myPets;
				} else {
					throw new PetPeersException("You don't have Pets");
				}
			} catch (PetPeersException e) {
				throw new PetPeersException("You don't have Pets");
			}
		} else {
			throw new PetPeersException("Invalid User");
		}
	}

	@Override
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class, Error.class })
	public Set<PetDto> loginUser(LoginValidator loginRequest) throws PetPeersException {
		User userExists = userRepository.findByUserNameAndUserPassword(loginRequest.getUserName(),
				loginRequest.getPassword());
		List<PetDto> pets = null;
		if (userExists != null) {
			pets = petClientInterface.getAllPets();
			if (pets != null && pets.size() > 0) {
				return (Set<PetDto>) pets;
			} else {
				throw new PetPeersException("No Pets Available");
			}
		} else {
			throw new PetPeersException("Either User Name or Password or both are invalid");
		}

	}
	@Override
	public User getUserById(Long userId) throws PetPeersException {
	User user = userRepository.findById(userId).get();
	if (user != null) {
	return user;
	} else {
	throw new PetPeersException("No user exists with given user id");
	}
	}
	
	@Override
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class, Error.class })
	public Set<User> listUsers() throws PetPeersException {
	Set<User> users = (Set<User>) userRepository.findAll();
	if (users != null && users.size() > 0) {
	return users;
	} else {
	throw new PetPeersException("No users found");
	}
	}
	
	@Override
	public void removeUser(Long userId) throws PetPeersException {
	User user = userRepository.findById(userId).get();
	if (user != null) {
	userRepository.deleteById(userId);
	} else {
	throw new PetPeersException("No user exists with given user id");
	}
	}
	
	@Override
	public User updateUser(Long userId, UserValidator userValidator) throws PetPeersException {
	User userUpdated = null;
	User user = null;
	if ((userRepository.findById(userId) != null)
	&& !(userValidator.getUserPassword().equals(userValidator.getConfirmPassword()))) {
	throw new PetPeersException("User Password and Confirm Password do not match");
	} else {
	User userExistsWithUserName = userRepository.findByUserName(userValidator.getUserName());
	if (userExistsWithUserName != null) {
	throw new PetPeersException("The User Name already exists. Please set a different User Name");
	} else {
	try {
	user = new User();
	user.setUserName(userValidator.getUserName());
	user.setUserPassword(userValidator.getUserPassword());
	userUpdated = userRepository.save(user);
	if (userUpdated != null) {
	return userUpdated;
	} else {
	throw new PetPeersException("User Not created");
	}
	} catch (Exception e) {
	logger.error("{}", e.getMessage());
	}
	}
	return userUpdated;
	}
	}
	
	@Override
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class, Error.class })
	public PetDto buyPet(Long userId, Long petId) throws PetPeersException {
		PetDto petWithOwnerCreated = null;
		Optional<User> user = userRepository.findById(userId);
		if (user.isPresent()) {
			Optional<PetDto> pet = petClientInterface.getPetById(petId);
			if (pet.isPresent()) {
				if (pet.get().getUserId() != null) {
					throw new PetPeersException("Pet Already Purchased Try with another Pet(PetId)");
				} else {
					pet.get().setUserId(userId);
					petWithOwnerCreated = petClientInterface.savePetWithUser(pet.get());
					if (petWithOwnerCreated != null) {
						return petWithOwnerCreated;
					} else {
						throw new PetPeersException("Entry Not created");
					}
				}
			} else {
				throw new PetPeersException("Pet Not exists with Given Id");
			}
		} else {
			throw new PetPeersException("User Not exists with Given Id");
		}

	}

	}

