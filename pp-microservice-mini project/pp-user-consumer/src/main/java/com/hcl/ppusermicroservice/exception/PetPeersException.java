package com.hcl.ppusermicroservice.exception;

public class PetPeersException extends Exception {

	


	private static final long serialVersionUID = -1625429763931436278L;
	private String customMessage;

	public PetPeersException(String customMessage) {
		super();
		this.customMessage = customMessage;
	}

	public String getCustomMessage() {
		return customMessage;
	}

	public void setCustomMessage(String customMessage) {
		this.customMessage = customMessage;
	}

	@Override
	public String getMessage() {
		return customMessage;
	}

}

