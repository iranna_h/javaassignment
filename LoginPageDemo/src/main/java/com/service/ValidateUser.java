package com.service;

/**
 * 
 * @author iranna
 *
 */
public interface ValidateUser {
	public abstract String validateUserIdAndPassword(String userId, String password);

}
