package com.main;

/**
 * 
 * @author iranna
 *
 */
import java.util.Scanner;

import com.service.ValidateUser;
import com.service.Validation;

public class MainApp {
	public static void main(String[] args) {
		ValidateUser validateUser = new Validation();
		Scanner scanner = new Scanner(System.in);
		String userId, password;

		System.out.println("enter user id");
		userId = scanner.nextLine();

		System.out.println("enter password");
		password = scanner.nextLine();

		validateUser.validateUserIdAndPassword(userId, password);

		validateUser = null;

	}

}
