package com.model;

/**
 * 
 * @author iranna
 *
 */
public class Customer {
	private int customerId;
	private String customerName;
	private Account[] axxount;

	public Customer() {
		super();

	}

	public Customer(int customerId, String customerName, Account[] axxount) {
		super();
		this.customerId = customerId;
		this.customerName = customerName;
		this.axxount = axxount;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Account[] getAccount() {
		return axxount;
	}

	public void setAccount(Account[] axxount) {
		this.axxount = axxount;
	}

}
