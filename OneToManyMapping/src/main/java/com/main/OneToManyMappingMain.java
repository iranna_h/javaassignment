package com.main;

/**
 * 
 * @author iranna
 *
 */
import com.model.Account;
import com.model.Customer;

public class OneToManyMappingMain {

	public static void main(String[] args) {
		Account account1 = new Account(1011, "SA");
		Account account2 = new Account(1012, "CA");

		Account[] accounts = new Account[2];
		accounts[0] = account1;
		accounts[1] = account2;

		Customer customer = new Customer();
		customer.setCustomerId(11);
		customer.setCustomerName("John");
		customer.setAccount(accounts);

		System.out.println("Customer has two accounts");
		System.out.println("John's account details");
		System.out.println("Customer no:   " + customer.getCustomerId());
		System.out.println("Customer name: " + customer.getCustomerName());

		Account[] accountArray = customer.getAccount();

		for (int i = 0; i < accountArray.length; i++) {
			System.out.println("account no:   " + accountArray[i].getAccountNumber());
			System.out.println("account name: " + accountArray[i].getAccountName());
		}

		account1 = null;
		account2 = null;
		accounts = null;
		customer = null;
	}

}
