package com.main;

/**
 * 
 * @author iranna
 *
 */
public class ImplementationFile implements InterfaceFile {
	public int getSum(int n1, int n2) {
		return n1 + n2;
	}
}
