package com.main;
/**
 * 
 * @author iranna
 *
 */
public interface InterfaceFile {
	public static final float PI = 3.1415f;

	public abstract int getSum(int n1, int n2);
}
