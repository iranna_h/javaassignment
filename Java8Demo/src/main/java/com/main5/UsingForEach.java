package com.main5;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author iranna
 *
 */
public class UsingForEach {

	public static void main(String[] args) {
		ShoppingCart shoppingCart1 = new ShoppingCart(1234, "Apples");
		ShoppingCart shoppingCart2 = new ShoppingCart(2234, "Grapes");
		List<ShoppingCart> cart = new ArrayList();
		cart.add(shoppingCart1);
		cart.add(shoppingCart2);
		System.out.println("Total iteams purchased are : " + cart.size() + "\n");
		cart.forEach((a) -> {
			System.out.println("Product Id	:" + a.getProductId());
			System.out.println("Product Name	:" + a.getProductName() + "\n");
		});
	}

}
