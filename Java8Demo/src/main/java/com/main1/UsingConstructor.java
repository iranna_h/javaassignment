package com.main1;

/**
 * 
 * @author iranna
 *
 */
public class UsingConstructor {

	public UsingConstructor(String str) {
		System.out.println(str + ", Learning Constructor Reference");
	}

}
