package com.main1;

/**
 * 
 * @author iranna
 *
 */
@FunctionalInterface
public interface UsingInterface {
	UsingConstructor match(String s);
}
