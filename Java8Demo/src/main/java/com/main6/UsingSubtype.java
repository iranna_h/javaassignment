package com.main6;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author iranna
 *
 */
public class UsingSubtype {

	public static void main(String[] args) {
		List<? super Number> list = new ArrayList<>();
		list.add(new Integer(7));
		list.add(new Float(7.34));
		list.forEach((a) -> {
			System.out.println("<? super Number> - accepts Integer and Float :" + a + "\n");
		});
	}

}
