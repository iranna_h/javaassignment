package com.main3;

/**
 * 
 * @author iranna
 *
 */
@FunctionalInterface
public interface UsingFunctionalInterface {
	public abstract void display();
}
