package com.main2;

/**
 * 
 * @author iranna
 *
 */
@FunctionalInterface
public interface UsingStaticInterface {
	public int add(int a, int b);
}
