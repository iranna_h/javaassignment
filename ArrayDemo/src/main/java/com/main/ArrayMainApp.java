package com.main;

/**
 * 
 * @author iranna
 *
 */
public class ArrayMainApp {

	public static void main(String[] args) {
		int[] intArray = { 1, 2, 3, 8 };
		int sum = 0;
		for (int i = 0; i < intArray.length; i++) {
			sum = sum + intArray[i];
		}
		System.out.println("sum of array elements is" + " " + sum);
	}

}
